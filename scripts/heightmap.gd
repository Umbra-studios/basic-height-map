@tool
extends StaticBody3D

@export_range(16,64) var CHUNK_SIZE:int
@export var heightmap_material:Material
@export var heightmap :Texture2D= Texture2D.new()
var shape :HeightMapShape3D= HeightMapShape3D.new()
var plane:PlaneMesh=PlaneMesh.new()
@onready var collisionshape:=$CollisionShape3D

@export var Scale:=10.0:
	set(val):
		Scale=val
		update()
func _ready():
	var a:Image
	if heightmap is NoiseTexture:
		a=heightmap.noise.get_seamless_image(512)
	else:
		a=heightmap.get_image()
	a.convert(Image.FORMAT_RF)
	shape.map_width = heightmap.get_width()
	shape.map_depth= heightmap.get_height()
	plane.size=Vector2(heightmap.get_width(),heightmap.get_height())
	plane.subdivide_depth= heightmap.get_height()
	plane.subdivide_width= heightmap.get_width()
	heightmap_material.set("shader_param/Scale",Scale)
	heightmap_material.set("shader_param/terrain",heightmap)
	var float_array :PackedFloat32Array= PackedFloat32Array()
	for y in a.get_height():
		for x in a.get_width():
			float_array.append((a.get_pixel(x, y).r-0.5)*Scale)
	shape.map_data=float_array
	collisionshape.shape=shape
	$MeshInstance3D.mesh=plane
func update():	
	if heightmap_material!=null:
		heightmap_material.set("shader_param/Scale",Scale)
		heightmap_material.set("shader_param/tex_vtx_5",heightmap)
		var a:Image
		if heightmap is NoiseTexture:
			a=heightmap.noise.get_seamless_image(512)
		else:
			a=heightmap.get_image()
		a.convert(Image.FORMAT_RF)
		var float_array :PackedFloat32Array= PackedFloat32Array()
		for y in a.get_height():
			for x in a.get_width():
				float_array.append((a.get_pixel(x, y).r-0.5)*Scale)
		shape.map_data=float_array
